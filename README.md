IconSharp: Cross-platform icon editor
=====================================
IconSharp is a cross-platform icon editor that was intended as a replacement for [KiconEdit][kicon]. It was started because there was a lack of decent properly-free (i.e. non-demo) icon editors, especially under Linux, and [Gimp][gimp] felt a too heavyweight for Icon production. The original aim was to include support for multi-icon [Microsoft ICO files][ico], but it soon became apparent that ICO support in itself would be a major development effort, so single-icon PNG-format icon editing was implemented instead. IconSharp was written using GTK# and although notionally a .NET application development was done using Linux rather than Windows.

![Screenshot](https://bitbucket.org/remyhorton/iconsharp/raw/master/iconsharp-rev39.png)

## Current status

Version 1 was released 5th May 2014 which at time of writing is almost exactly four years ago, and since then it has not been actively maintained nor developed. Development was abandoned because the C# [bindings for GTK][bindings] themselves were not being maintained, and from what I remember there were issues with ``v2.20`` that it supported. 

## Running IconSharp
Running IconSharp requires a platform capable of running .NET [Common Language Runtime][clr] binaries, as well as having the GTK# run-time libraries installed. Although notionally a .NET based program, IconSharp was developed using MonoDevelop under Linux, so there should be no problems running it on any non-Windows operating system.

Running using .NET run-time
: IconSharp requires .NET v4, which on Windows7 requires installation of the [.NET run-time][net]. Windows8 includes .NET v4 as standard, but IconSharp has not been tested on this operating system. IconSharp also requires the [GTK# for .NET][gtk#] run-time libraries from Xamarin.

Running using Mono on Windows
: As an alternative to .NET, Windows users can install [Mono for Windows][mono]. This may be favourable in circumstances (e.g. no administrator privileges) where installation of the .NET v4 run-time is undesirable or impossible. The only complication is that Mono will not register itself as the execution environment for .NET binaries in the same way the Java run-time is auto-associated with .jar files. Therefore you need to either drag-and-drop the IconSharp binary onto ```mono.exe``` (or a shortcut to it), or specify IconSharp as a command-line parameter for Mono. 

Running on Ubuntu 12.04
: Under Ubuntu 12.04 you need to install the ```mono-complete``` package, as a bare-bones Mono install does not include libraries required to run IconSharp. If you only have the basic libraries installed, you will likely get errors like _Unhandled Exception: System.IO.FileNotFoundException: Could not load file or assembly_.

## Licence
IconSharp is licenced under [version 2 of the GPL][gpl2].

## Contact
Send emails to ``remy.horton`` (at) ``gmail.com``

[kicon]: http://www.kde.org/applications/graphics/kiconedit/
[ico]: http://en.wikipedia.org/wiki/ICO_(file_format)
[gimp]: http://www.gimp.org/
[clr]: http://en.wikipedia.org/wiki/Common_Language_Runtime
[net]: http://www.microsoft.com/en-gb/download/details.aspx?id=17718
[gtk#]: http://download.xamarin.com/GTKforWindows/Windows/gtk-sharp-2.12.21.msi
[mono]: http://www.go-mono.com/mono-downloads/download.html
[gpl2]: https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
[bindings]: https://www.gtk.org/language-bindings.php
