// IconSharp icon editor
// Copyright (C) 2014, Remy Horton.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


using System;
using Gtk;

namespace IconSharp
{

/// <summary>
/// Drawing buffer.
/// </summary>
/// <description>
/// This handles the scaling and clipping of the icon for display 
/// on a window. It is intended to avoid an oversized intermediate 
/// bitmap at high zoom levels on a small window.
/// </description>
class DrawBuffer
    {
    private Gdk.Pixbuf pbPixels;
    private byte[] pcxPixels;
    private int width;
    private int height;
    private int scale;
    private int sizeZoomedX;
    private int sizeZoomedY;

    /// <summary>
    /// Initializes a new instance of the <see cref="IconSharp.DrawBuffer"/> class.
    /// </summary>
    /// <param name="width">Initial width.</param>
    /// <param name="height">Initial height.</param>
    public DrawBuffer(int width, int height)
        {
        this.width = 0;
        this.height = 0;
        this.increaseSize(width, height);
        this.scale = 2;
        }

    /// <summary>
    /// Checks whether buffer is big enough for given icon size.
    /// </summary>
    /// <returns><c>true</c>, if buffer needed resizing, <c>false</c> otherwise.</returns>
    /// <param name="newWidth">New width.</param>
    /// <param name="newHeight">New height.</param>
    public bool increaseSize(int newWidth, int newHeight)
        {
        if(this.width < newWidth || this.height < newHeight)
            {
            this.pcxPixels = new byte[newWidth * newHeight * 4];

            for(int x=0; x<newWidth * newHeight * 4; x+=4)
                {
                this.pcxPixels[x + 0] = 128;
                this.pcxPixels[x + 1] = 128;
                this.pcxPixels[x + 2] = 128;
                this.pcxPixels[x + 3] = 255;
                }
            this.pbPixels = new Gdk.Pixbuf(this.pcxPixels, true, 8, newWidth, newHeight, newWidth * 4);
            this.width = newWidth;
            this.height = newHeight;
            return true;
            }
        return false;
        }

    /// <summary>
    /// Draws internal buffer to given window.
    /// </summary>
    /// <param name="hWnd">Target window.</param>
    /// <param name="hDC">Device Context of target window.</param>
    public void DrawTo(Gdk.Window hWnd, Gdk.GC hDC)
        {
        int sizeWndX;
        int sizeWndY;
        hWnd.GetSize(out sizeWndX, out sizeWndY);
        hDC.RgbFgColor = new Gdk.Color(128, 128, 128);
        hWnd.DrawRectangle(hDC, true, new Gdk.Rectangle(0, 0, sizeWndX, sizeWndY));
        hWnd.DrawPixbuf(
            hDC, this.pbPixels, 
            0, 0, 0, 0, this.sizeZoomedX, this.sizeZoomedY,
            Gdk.RgbDither.None, 0, 0);
        }

    /// <summary>
    /// Copies & scales a section of the given icon to internal buffer.
    /// </summary>
    /// <param name="pbIcon">Icon to draw</param>
    /// <param name="offsetX">Scrollbar offset in X direction.</param>
    /// <param name="offsetY">Scrollbar offset in Y direction.</param>
    /// <description>
    /// This draws the icon taking account of zoom level, and if zoomed icon
    /// is larger than the display area, account of where the scollbars are.
    /// </description>
    public void showSection(Gdk.Pixbuf pbIcon, int offsetX, int offsetY)
        {
        this.sizeZoomedX = System.Math.Min(pbIcon.Width * this.scale, this.pbPixels.Width);
        this.sizeZoomedY = System.Math.Min(pbIcon.Width * this.scale, this.pbPixels.Height);
        pbIcon.CompositeColor(
            this.pbPixels, 
            0, 0, this.sizeZoomedX, this.sizeZoomedY,
            -offsetX, -offsetY, this.scale, this.scale,
            Gdk.InterpType.Nearest, 255,
            0, 0, 64, 0xaaaaaa, 0xdddddd);
        }

    /// <summary>
    /// Gets or sets zoom level
    /// </summary>
    /// <value>Zoom level.</value>
    public int Zoom
        {
        get
            {
            return this.scale;
            }
        set
            {
            this.scale = value;
            }
        }

    /// <summary>
    /// Adjusts zoom incrementally.
    /// </summary>
    /// <param name="delta">Amount & direction to change zoom in.</param>
    public void adjustZoom(int delta)
        {
        if(delta < 0)
            {
            if(this.scale > -delta && this.scale > 2)
                this.scale += delta;
            }
        else
            {
            if(this.scale < 64)
                this.scale += delta;
            }
        }
    }

}

