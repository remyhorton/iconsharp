// IconSharp icon editor
// Copyright (C) 2014, Remy Horton.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


using System;
using Gtk;
using Glade;

namespace IconSharp
{

/// <summary>
/// Drawing modes.
/// </summary>
public enum DrawMode
    {
        None, Pencil, Pipette, Flood, Line,
        Circle, FilledCircle, Rect, FilledRect,
        Eraser, Select, EraserBig, Paste
    };

/// <summary>
/// Main program logic.
/// </summary>
class IconSharp
    {
    [Widget] Gtk.MenuItem menuNew16;
    [Widget] Gtk.MenuItem menuNew24;
    [Widget] Gtk.MenuItem menuNew32;
    [Widget] Gtk.MenuItem menuNew48;
    [Widget] Gtk.MenuItem menuNew64;
    [Widget] Gtk.MenuItem menuNew256;

    [Widget] Gtk.ImageMenuItem menuitemNew;
    [Widget] Gtk.ImageMenuItem menuitemOpen;
    [Widget] Gtk.ImageMenuItem menuitemSave;
    [Widget] Gtk.ImageMenuItem menuitemSaveAs;
    [Widget] Gtk.ImageMenuItem menuitemExit;
    [Widget] Gtk.ImageMenuItem menuitemCut;
    [Widget] Gtk.ImageMenuItem menuitemCopy;
    [Widget] Gtk.ImageMenuItem menuitemPaste;
    [Widget] Gtk.ImageMenuItem menuitemDelete;
    [Widget] Gtk.ImageMenuItem menuitemAbout;
    [Widget] Gtk.CheckMenuItem menuitemGrid;
    [Widget] Gtk.ImageMenuItem menuitemUndo;
    [Widget] Gtk.ImageMenuItem menuitemRedo;

    // cat gui.glade | grep GtkToolButton | awk -F\" '{print "[Widget] Gtk.ToolButton " $4 ";" }'
    [Widget] Gtk.Window       wndMain;
    [Widget] Gtk.EventBox     eventboxEdit;
    [Widget] Gtk.EventBox     eventboxColours;
    [Widget] Gtk.DrawingArea  drawEdit;
    [Widget] Gtk.DrawingArea  drawPreview;
    [Widget] Gtk.DrawingArea  drawColours;
    [Widget] Gtk.ToolButton toolPencil;
    [Widget] Gtk.ToolButton toolFlood;
    [Widget] Gtk.ToolButton toolPipette;
    [Widget] Gtk.ToolButton toolRect;
    [Widget] Gtk.ToolButton toolCircle;
    [Widget] Gtk.ToolButton toolLine;
    [Widget] Gtk.ToolButton toolRectFilled;
    [Widget] Gtk.ToolButton toolCircleFilled;
    [Widget] Gtk.ToolButton toolEraser;
    [Widget] Gtk.ToolButton toolEraserBig;
    [Widget] Gtk.ToolButton toolSelect;
    [Widget] Gtk.ToolButton toolCut;
    [Widget] Gtk.ToolButton toolCopy;
    [Widget] Gtk.ToolButton toolPaste;
    [Widget] Gtk.ToolButton toolZoomOut;
    [Widget] Gtk.ToolButton toolZoomBest;
    [Widget] Gtk.ToolButton toolZoomIn;

    [Widget] Gtk.VScrollbar vscrollEdit;
    [Widget] Gtk.HScrollbar hscrollEdit;

    [Widget] Gtk.ColorButton btnColour;

    private Gdk.GC gcPreview;
    private Gdk.Window wndDrawPanel;
    private Gdk.GC gcDrawPanel;
    private Gdk.Point posStart;
    private Gdk.Point posEnd;
    private Gdk.Pixbuf pbClipboard;

    private DrawBuffer iconDrawBuffer;
    private Icon iconCurrent;
    private DrawMode mode;
    private bool isDrawing;
    private bool isUnsaved;
    private string strFilename;

    private Gdk.Color colourGuides;
    private Gdk.Color colourSelect;
    private PaletteManager palette;

    private Gdk.Cursor cursorPencil;
    private Gdk.Cursor cursorCircle;
    private Gdk.Cursor cursorFlood;
    private Gdk.Cursor cursorEraser;
    private Gdk.Cursor cursorEraserBig;
    private Gdk.Cursor cursorFilledCircle;
    private Gdk.Cursor cursorFilledRect;
    private Gdk.Cursor cursorLine;
    private Gdk.Cursor cursorPipette;
    private Gdk.Cursor cursorRect;
    private Gdk.Cursor cursorSelect;
    private Gdk.Cursor cursorPaste;

    
    /// <summary>
    /// Displays warning about unimplemented feature.
    /// </summary>
    /// <param name="message">Message to show.</param>
    private void unimplementedFeature(string message)
        {
        MessageDialog dialog = new MessageDialog(
            this.wndMain, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, message);
        dialog.Destroy();
        // throw new NotImplementedException();
        }

    /// <summary>
    /// Main program entrypoint.
    /// </summary>
    /// <param name="args">The command-line arguments (unused).</param>
    public static void Main (string[] args)
        {
        new IconSharp (args);
        }
    
    private void LoadIconIntoFactory(Gtk.IconFactory icons, string strName, string strStockId)
        {
        Gdk.Pixbuf iconResource = Gdk.Pixbuf.LoadFromResource(strName);
        Gtk.IconSet icon = new IconSet(iconResource);
        icons.Add(strStockId, icon);
        }

    /// <summary>
    /// Loads the icon resources used in the tool palette.
    /// </summary>
    private void LoadIconResources()
        {
        Gtk.IconFactory icons = new IconFactory();
        this.LoadIconIntoFactory(icons, "iconPencil.png",       "tool-pencil");
        this.LoadIconIntoFactory(icons, "iconFlood.png",        "tool-flood");
        this.LoadIconIntoFactory(icons, "iconLine.png",         "tool-line");
        this.LoadIconIntoFactory(icons, "iconCircle.png",       "tool-circle");
        this.LoadIconIntoFactory(icons, "iconCircleFilled.png", "tool-circle-filled");
        this.LoadIconIntoFactory(icons, "iconRect.png",         "tool-rect");
        this.LoadIconIntoFactory(icons, "iconRectFilled.png",   "tool-rect-filled");
        this.LoadIconIntoFactory(icons, "iconEraser.png",       "tool-eraser");
        this.LoadIconIntoFactory(icons, "iconEraserBig.png",    "tool-eraser-big");
        this.LoadIconIntoFactory(icons, "iconSelect.png",       "tool-select");
        icons.AddDefault();
        }

    /// <summary>
    /// Loads the cursors used for various drawing modes.
    /// </summary>
    private void LoadCursors()
        {
        this.cursorPencil       = new Gdk.Cursor(Gdk.CursorType.Pencil);
        this.cursorFlood        = new Gdk.Cursor(Gdk.CursorType.Spraycan);
        this.cursorLine         = new Gdk.Cursor(Gdk.CursorType.TopLeftArrow);
        this.cursorPipette      = new Gdk.Cursor(Gdk.CursorType.Target);

        this.cursorCircle       = new Gdk.Cursor(Gdk.CursorType.UlAngle);
        this.cursorFilledCircle = new Gdk.Cursor(Gdk.CursorType.UlAngle);
        this.cursorRect         = new Gdk.Cursor(Gdk.CursorType.UlAngle);
        this.cursorFilledRect   = new Gdk.Cursor(Gdk.CursorType.UlAngle);

        this.cursorEraser       = new Gdk.Cursor(Gdk.CursorType.Pencil);
        this.cursorEraserBig    = new Gdk.Cursor(Gdk.CursorType.DrapedBox);

        this.cursorSelect       = new Gdk.Cursor(Gdk.CursorType.TopLeftCorner);
        this.cursorPaste        = new Gdk.Cursor(Gdk.CursorType.Arrow);
        }

    /// <summary>
    /// Creates the main GUI object.
    /// </summary>
    /// <param name="args">Arguments (unused).</param>
    public IconSharp(string[] args)
        {
        Application.Init ();
        this.LoadIconResources();
        this.LoadCursors();
        //Glade.XML xml = new Glade.XML ("/home/remy/C#/Mono/IconSharp/IconSharp/gui.glade", "wndMain", null);
        Glade.XML xml = new Glade.XML(null, "gui.glade", "wndMain", null);
        xml.Autoconnect(this);
        this.wndMain.ShowAll();

        this.palette = new PaletteManager(this.drawColours, this.eventboxColours, this.OnPaletteSelection);

        this.Mode = DrawMode.Pencil;
        this.isDrawing = false;
        this.isUnsaved = false;
        this.pbClipboard = null;
        this.strFilename = null;

        this.iconDrawBuffer = new DrawBuffer(512, 512);
        this.iconCurrent = new Icon(64, 64, 8, this.drawEdit.GdkWindow);
        this.iconDrawBuffer.Zoom = 4;

        this.wndDrawPanel = this.drawEdit.GdkWindow;
        this.gcDrawPanel = new Gdk.GC(this.wndDrawPanel);
        this.gcPreview = new Gdk.GC(this.drawPreview.GdkWindow);

        this.colourGuides = new Gdk.Color(0, 0, 0);
        this.colourSelect = new Gdk.Color(64, 64, 64);

        this.DoRedraw();
        this.setScrollBars();

        #region Events
        this.drawEdit.ConfigureEvent += this.OnResize;
        
        this.eventboxEdit.EnterNotifyEvent += this.OnMouseOver;
        this.eventboxEdit.LeaveNotifyEvent += this.OnMouseGone;

        this.vscrollEdit.ChangeValue += this.ScrollBarMove;
        this.hscrollEdit.ChangeValue += this.ScrollBarMove;
    
        this.drawEdit.ExposeEvent    += this.OnExpose;
        this.drawPreview.ExposeEvent += this.OnExposePreview;

        this.eventboxEdit.ButtonPressEvent += this.OnMousePress;
        this.eventboxEdit.MotionNotifyEvent += this.OnMouseMove;
        this.eventboxEdit.ButtonReleaseEvent += this.OnMouseRelease;

        this.wndMain.DeleteEvent  += this.OnDeleteEvent;

        this.toolPencil.Clicked   += this.OnToolPress;
        this.toolFlood.Clicked    += this.OnToolPress;
        this.toolPipette.Clicked  += this.OnToolPress;
        this.toolRect.Clicked     += this.OnToolPress;
        //this.toolCircle.Clicked   += this.OnToolPress;
        this.toolLine.Clicked     += this.OnToolPress;
        this.toolRectFilled.Clicked   += this.OnToolPress;
        //this.toolCircleFilled.Clicked += this.OnToolPress;
        this.toolEraser.Clicked   += this.OnToolPress;
        this.toolEraserBig.Clicked    += this.OnToolPress;
        this.toolSelect.Clicked   += this.OnToolPress;
        this.toolCut.Clicked      += this.OnToolPress;
        this.toolCopy.Clicked     += this.OnToolPress;
        this.toolPaste.Clicked    += this.OnToolPress;

        this.toolZoomOut.Clicked  += this.OnToolZoom;
        this.toolZoomBest.Clicked += this.OnToolZoom;
        this.toolZoomIn.Clicked   += this.OnToolZoom;

        this.btnColour.ColorSet   += this.OnColourChange;

        this.menuNew16.Activated += this.OnMenuNew;
        this.menuNew24.Activated += this.OnMenuNew;
        this.menuNew32.Activated += this.OnMenuNew;
        this.menuNew48.Activated += this.OnMenuNew;
        this.menuNew64.Activated += this.OnMenuNew;
        this.menuNew256.Activated += this.OnMenuNew;

        //this.menuitemNew.Activated    += this.OnMenuItem;
        this.menuitemOpen.Activated   += this.OnMenuItem;
        this.menuitemSave.Activated   += this.OnMenuItem;
        this.menuitemSaveAs.Activated += this.OnMenuItem;
        this.menuitemExit.Activated   += this.OnMenuItem;
        this.menuitemCut.Activated    += this.OnMenuItem;
        this.menuitemCopy.Activated   += this.OnMenuItem;
        this.menuitemPaste.Activated  += this.OnMenuItem;
        this.menuitemDelete.Activated += this.OnMenuItem;
        this.menuitemAbout.Activated  += this.OnMenuAbout;

        this.menuitemGrid.Activated   += this.OnMenuGrid;

        this.menuitemUndo.Activated   += this.OnMenuUndo;
        this.menuitemRedo.Activated   += this.OnMenuRedo;
        #endregion

        Application.Run();
        }

    /// <summary>
    /// Sets the drawing mode.
    /// </summary>
    /// <value>New drawing mode.</value>
    /// <description>
    /// This has the side-effect of selecting the correct
    /// cursor for the mode.
    /// </description>
    public DrawMode Mode
        {
        set
            {
            this.mode = value;
            switch(value)
                {
                case DrawMode.Pencil:
                    this.drawEdit.GdkWindow.Cursor = this.cursorPencil;
                    break;
                case DrawMode.Flood:
                    this.drawEdit.GdkWindow.Cursor = this.cursorFlood;
                    break;
                case DrawMode.Pipette:
                    this.drawEdit.GdkWindow.Cursor = this.cursorPipette;
                    break;
                case DrawMode.Line:
                    this.drawEdit.GdkWindow.Cursor = this.cursorLine;
                    break;
                case DrawMode.Circle:
                    this.drawEdit.GdkWindow.Cursor = this.cursorCircle;
                    break;
                case DrawMode.Rect:
                    this.drawEdit.GdkWindow.Cursor = this.cursorRect;
                    break;
                case DrawMode.FilledCircle:
                    this.drawEdit.GdkWindow.Cursor = this.cursorFilledCircle;
                    break;
                case DrawMode.FilledRect:
                    this.drawEdit.GdkWindow.Cursor = this.cursorFilledRect;
                    break;
                case DrawMode.Eraser:
                    this.drawEdit.GdkWindow.Cursor = this.cursorEraser;
                    break;
                case DrawMode.EraserBig:
                    this.drawEdit.GdkWindow.Cursor = this.cursorEraserBig;
                    break;
                case DrawMode.Select:
                    this.drawEdit.GdkWindow.Cursor = this.cursorSelect;
                    break;
                case DrawMode.Paste:
                    this.drawEdit.GdkWindow.Cursor = this.cursorPaste;
                    break;
                default:
                    this.unimplementedFeature("Requested nonexistent drawing mode");
                    break;
                }
            }
        }

    /// <summary>
    /// Callback for colopur selection on palette
    /// </summary>
    /// <param name="colourNew">Newly selected colour</param>
    protected void OnPaletteSelection(Gdk.Color colourNew)
        {
        this.btnColour.Color = colourNew;
        this.iconCurrent.colourCurrent = colourNew;
        }

    /// <summary>
    /// Reconfigure scroll bars based new Icon/Window size & zoom
    /// </summary>
    protected void setScrollBars()
        {
        int sizeWndX;
        int sizeWndY;
        this.wndDrawPanel.GetSize(out sizeWndX, out sizeWndY);
        this.vscrollEdit.SetRange(0, this.iconCurrent.height * this.iconDrawBuffer.Zoom);
        this.vscrollEdit.SetIncrements(1, 5);
        this.vscrollEdit.Adjustment.PageSize = sizeWndY;
        this.vscrollEdit.Value = 0;
        this.hscrollEdit.SetRange(0, this.iconCurrent.width * this.iconDrawBuffer.Zoom);
        this.hscrollEdit.SetIncrements(1, 5);
        this.hscrollEdit.Adjustment.PageSize = sizeWndX;
        this.hscrollEdit.Value = 0;
        }

    /// <summary>
    /// Refreshes the icon drawing area.
    /// </summary>
    protected void DoRedraw()
        {
        this.iconDrawBuffer.showSection(
            this.iconCurrent.getPixelBuff(),
            Convert.ToInt32(this.hscrollEdit.Value),
            Convert.ToInt32(this.vscrollEdit.Value)
            );
        this.drawEdit.QueueDraw();
        }

    protected void ScrollBarMove(object sender, ChangeValueArgs args)
        {
        this.DoRedraw();
        }
    
    /// <summary>
    /// Application window resized.
    /// </summary>
    /// <param name="sender">Sender (unused).</param>
    /// <param name="args">Resize arguments (used to get new size).</param>
    protected void OnResize(object sender, ConfigureEventArgs args)
        {
        this.iconDrawBuffer.increaseSize(args.Event.Width, args.Event.Height);
        this.setScrollBars();
        this.DoRedraw();
        }
    
    /// <summary>
    /// Mouse button released over drawing area.
    /// </summary>
    /// <param name="sender">Unused.</param>
    /// <param name="args">Unused.</param>
    /// <description>This causes the current state of drawing in the scratch
    /// buffer to be committed to the icon. 
    /// </description>
    protected void OnMouseRelease(object sender, ButtonReleaseEventArgs args)
        {
        if( this.isDrawing )
            {
            bool isErase = (this.mode == DrawMode.Eraser || this.mode == DrawMode.EraserBig) ? true : false;
            switch( this.mode )
                {
                case DrawMode.Paste:
                    this.Mode = DrawMode.Pencil;
                    // Stupidly, C# does now alow fall-thru from non-empty
                    // case-statements, so need to duplicate the code here..
                    this.iconCurrent.commitShape(isErase);
                    this.drawEdit.QueueDraw();
                    this.isDrawing = false;
                    break;

                case DrawMode.Pencil:
                case DrawMode.Line:
                case DrawMode.Rect:
                case DrawMode.FilledRect:
                case DrawMode.Eraser:
                case DrawMode.EraserBig:
                    this.iconCurrent.commitShape(isErase);
                    this.drawEdit.QueueDraw();
                    this.isDrawing = false;
                    break;

                case DrawMode.Select:
                    this.drawEdit.QueueDraw();
                    //this.isDrawing = false;
                    break;

                case DrawMode.Circle:
                case DrawMode.FilledCircle:
                    this.unimplementedFeature("MouseRelease: Circles not supported");
                    break;

                default:
                    this.unimplementedFeature("MouseRelease: Unimplemented case");
                    break;
                }
            }
        }

    /// <summary>
    /// Mouse pressed over drawing area.
    /// </summary>
    /// <param name="sender">Sender (unused).</param>
    /// <param name="args">Mouse arguments (used to get mouse start position).</param>
    /// <description>
    /// This records the initial position of the mouse and fires off a redraw.
    /// </description>
    protected void OnMousePress(object sender, ButtonPressEventArgs args)
        {
        this.isDrawing = true;
        this.isUnsaved = true;

        int posX = (Convert.ToInt32(args.Event.X)+Convert.ToInt32(this.hscrollEdit.Value)) / this.iconDrawBuffer.Zoom;
        int posY = (Convert.ToInt32(args.Event.Y)+Convert.ToInt32(this.vscrollEdit.Value)) / this.iconDrawBuffer.Zoom;

        this.posStart.X = Convert.ToInt32(args.Event.X) + Convert.ToInt32(this.hscrollEdit.Value);
        this.posStart.Y = Convert.ToInt32(args.Event.Y) + Convert.ToInt32(this.vscrollEdit.Value);
        this.posEnd.X = this.posStart.X;
        this.posEnd.Y = this.posStart.Y;

        Gdk.Point posHere;
        posHere.X = this.posStart.X / this.iconDrawBuffer.Zoom;
        posHere.Y = this.posStart.Y / this.iconDrawBuffer.Zoom;
        switch( this.mode )
            {
            case DrawMode.Pencil:
            case DrawMode.Line:
            case DrawMode.Rect:
            case DrawMode.FilledRect:
            case DrawMode.Select:
            case DrawMode.Eraser:
            case DrawMode.EraserBig:
            case DrawMode.Circle:
            case DrawMode.FilledCircle:
            case DrawMode.Paste:
                this.iconCurrent.plotShape(this.mode, posHere, posHere, this.pbClipboard);
                break;

            case DrawMode.Flood:
                this.isDrawing = false;
                this.iconCurrent.plotShape(this.mode, posHere, posHere, this.pbClipboard);
                this.iconCurrent.commitShape(false);
                this.drawPreview.QueueDraw();
                break;

            case DrawMode.Pipette:
                this.isDrawing = false;
                this.iconCurrent.colourCurrent = this.iconCurrent.getPixel(posX, posY);
                this.btnColour.Color = this.iconCurrent.colourCurrent;
                this.palette.Colour = this.iconCurrent.colourCurrent;
                this.drawColours.QueueDraw();
                break;

            default:
                this.unimplementedFeature("MousePress: Unimplemented case");
                break;
            }

        this.DoRedraw();
        }

    /// <summary>
    /// Mouse moved over drawing area.
    /// </summary>
    /// <param name="sender">Unused.</param>
    /// <param name="args">Mouse motion arguments. Used to get mounse position.</param>
    /// <description> This updates the mouse position, and either updates or adds to
    /// the scratch buffer as appropriate. 
    /// </description>
    protected void OnMouseMove(object sender, MotionNotifyEventArgs args)
        {
        int posX = Convert.ToInt32(args.Event.X) + Convert.ToInt32(this.hscrollEdit.Value);
        int posY = Convert.ToInt32(args.Event.Y) + Convert.ToInt32(this.vscrollEdit.Value);
        if( (args.Event.State & Gdk.ModifierType.Button1Mask) == 0 && this.mode != DrawMode.Paste)
            {
            return;
            }

        if( this.isDrawing )
            {
            this.posEnd.X = posX;
            this.posEnd.Y = posY;

            if( posX < 0 || posX >= this.iconCurrent.width * this.iconDrawBuffer.Zoom )
                return;
            if( posY < 0 || posY >= this.iconCurrent.height * this.iconDrawBuffer.Zoom )
                return;
            this.DoRedraw();
            }
        }

    protected void OnMouseOver(object sender, EnterNotifyEventArgs args)
        {
        }

    protected void OnMouseGone(object sender, LeaveNotifyEventArgs args)
        {
        }

    protected void drawGrid()
        {
        int posX = this.iconDrawBuffer.Zoom;
        int posY = this.iconDrawBuffer.Zoom * this.iconCurrent.height;
        this.gcDrawPanel.RgbFgColor = new Gdk.Color(0, 0, 0);
        this.gcDrawPanel.SetLineAttributes(
            1, Gdk.LineStyle.Solid, Gdk.CapStyle.Butt, Gdk.JoinStyle.Miter);
        while( posX < this.iconCurrent.width * this.iconDrawBuffer.Zoom )
            {
            this.wndDrawPanel.DrawLine(
                this.gcDrawPanel, 
                posX, 0, posX, posY);
            posX += this.iconDrawBuffer.Zoom;
            }
        posX = this.iconDrawBuffer.Zoom * this.iconCurrent.width;
        posY = this.iconDrawBuffer.Zoom;
        while( posY < this.iconCurrent.width * this.iconDrawBuffer.Zoom )
            {
            this.wndDrawPanel.DrawLine(
                this.gcDrawPanel, 
                0, posY, posX, posY);
            posY += this.iconDrawBuffer.Zoom;
            }
        }

    /// <summary>
    /// Draws the overlay guides for lines, squares, and selection.
    /// </summary>
    /// <description>
    /// I am not actually sure if drawing these overlays in addition to the actual
    /// lines in the scratch buffer actually gains anything. 
    /// </description>
    protected void drawShapeGuides()
        {
        Gdk.Point[] lstPoints;
        this.gcDrawPanel.RgbFgColor = this.colourGuides;
        switch( this.mode )
            {
            case DrawMode.None:
                break;

            case DrawMode.Pencil:
            case DrawMode.Eraser:
            case DrawMode.EraserBig:
                break;

            case DrawMode.Line:
                this.gcDrawPanel.SetLineAttributes(
                    2, Gdk.LineStyle.OnOffDash, Gdk.CapStyle.Butt, Gdk.JoinStyle.Miter);
                Gdk.Point[] points = new Gdk.Point[2] { this.posStart, this.posEnd };

                // Center guide line ends on 

                points[0].X -= + Convert.ToInt32(this.hscrollEdit.Value);
                points[1].X -= + Convert.ToInt32(this.hscrollEdit.Value);
                points[0].Y -= + Convert.ToInt32(this.vscrollEdit.Value);
                points[1].Y -= + Convert.ToInt32(this.vscrollEdit.Value);

                points[0].Y -= points[0].Y % this.iconDrawBuffer.Zoom;
                points[0].Y += this.iconDrawBuffer.Zoom / 2;
                points[0].X -= points[0].X % this.iconDrawBuffer.Zoom;
                points[0].X += this.iconDrawBuffer.Zoom / 2;
                points[1].Y -= points[1].Y % this.iconDrawBuffer.Zoom;
                points[1].Y += this.iconDrawBuffer.Zoom / 2;
                points[1].X -= points[1].X % this.iconDrawBuffer.Zoom;
                points[1].X += this.iconDrawBuffer.Zoom / 2;

                this.wndDrawPanel.DrawLines(this.gcDrawPanel, points);
                break;

            case DrawMode.Rect:
            case DrawMode.FilledRect:
                this.gcDrawPanel.SetLineAttributes(
                    1, Gdk.LineStyle.Solid, Gdk.CapStyle.Butt, Gdk.JoinStyle.Miter);
                lstPoints = new Gdk.Point[] {
                    new Gdk.Point( posStart.X, posStart.Y ),
                    new Gdk.Point( posEnd.X,   posStart.Y ),
                    new Gdk.Point( posEnd.X,   posEnd.Y ),
                    new Gdk.Point( posStart.X, posEnd.Y ),
                    new Gdk.Point( posStart.X, posStart.Y )
                };
                for(int idx=0; idx < lstPoints.Length; idx++)
                    {
                    lstPoints[idx].X -= Convert.ToInt32(this.hscrollEdit.Value);
                    lstPoints[idx].Y -= Convert.ToInt32(this.vscrollEdit.Value);
                    }
                for(int idxPoint=0; idxPoint<lstPoints.Length; idxPoint++)
                    {
                    lstPoints[idxPoint].X -= lstPoints[idxPoint].X % this.iconDrawBuffer.Zoom;
                    lstPoints[idxPoint].X += this.iconDrawBuffer.Zoom >> 1;
                    lstPoints[idxPoint].Y -= lstPoints[idxPoint].Y % this.iconDrawBuffer.Zoom;
                    lstPoints[idxPoint].Y += this.iconDrawBuffer.Zoom >> 1;
                    }
                this.wndDrawPanel.DrawLines(this.gcDrawPanel, lstPoints);
                break;

            case DrawMode.Select:
                this.gcDrawPanel.RgbFgColor = this.colourSelect;
                this.gcDrawPanel.SetLineAttributes(
                    2, Gdk.LineStyle.OnOffDash, Gdk.CapStyle.Butt, Gdk.JoinStyle.Miter);
                lstPoints = new Gdk.Point[] {
                    new Gdk.Point( posStart.X, posStart.Y ),
                    new Gdk.Point( posEnd.X,   posStart.Y ),
                    new Gdk.Point( posEnd.X,   posEnd.Y ),
                    new Gdk.Point( posStart.X, posEnd.Y ),
                    new Gdk.Point( posStart.X, posStart.Y )
                };
                for(int idxPoint=0; idxPoint<lstPoints.Length; idxPoint++)
                    {
                    lstPoints[idxPoint].X -= lstPoints[idxPoint].X % this.iconDrawBuffer.Zoom;
                    lstPoints[idxPoint].X += this.iconDrawBuffer.Zoom >> 1;
                    lstPoints[idxPoint].Y -= lstPoints[idxPoint].Y % this.iconDrawBuffer.Zoom;
                    lstPoints[idxPoint].Y += this.iconDrawBuffer.Zoom >> 1;
                    }
                this.wndDrawPanel.DrawLines(this.gcDrawPanel, lstPoints);
                break;

            case DrawMode.Circle:
            case DrawMode.FilledCircle:
                this.gcDrawPanel.SetLineAttributes(
                    1, Gdk.LineStyle.Solid, Gdk.CapStyle.Butt, Gdk.JoinStyle.Miter);
                lstPoints = new Gdk.Point[] {
                    new Gdk.Point( posStart.X, posStart.Y ),
                    new Gdk.Point( posEnd.X,   posStart.Y ),
                    new Gdk.Point( posEnd.X,   posEnd.Y ),
                    new Gdk.Point( posStart.X, posEnd.Y ),
                    new Gdk.Point( posStart.X, posStart.Y )
                };
                for(int idxPoint=0; idxPoint<lstPoints.Length; idxPoint++)
                    {
                    lstPoints[idxPoint].X -= lstPoints[idxPoint].X % this.iconDrawBuffer.Zoom;
                    lstPoints[idxPoint].X += this.iconDrawBuffer.Zoom >> 1;
                    lstPoints[idxPoint].Y -= lstPoints[idxPoint].Y % this.iconDrawBuffer.Zoom;
                    lstPoints[idxPoint].Y += this.iconDrawBuffer.Zoom >> 1;
                    }
                this.wndDrawPanel.DrawLines(this.gcDrawPanel, lstPoints);
                break;

            case DrawMode.Flood:
            case DrawMode.Pipette:
                this.unimplementedFeature("This point should not be reached..");
                break;
        
            case DrawMode.Paste:
                break;

            default:
                this.unimplementedFeature("drawShapeGuides: unimplemented case");
                break;
            }
        }

    /// <summary>
    /// Refreshes the drawing area.
    /// </summary>
    /// <param name="o">Unused.</param>
    /// <param name="args">Unused.</param>
    protected void OnExpose (object o, ExposeEventArgs args)
        {
        if(this.isDrawing)
            {
            Gdk.Point posS;
            Gdk.Point posE;
            posS.X = this.posStart.X / this.iconDrawBuffer.Zoom;
            posS.Y = this.posStart.Y / this.iconDrawBuffer.Zoom;
            posE.X = this.posEnd.X / this.iconDrawBuffer.Zoom;
            posE.Y = this.posEnd.Y / this.iconDrawBuffer.Zoom;
            this.iconCurrent.plotShape(this.mode, posS, posE, this.pbClipboard);
            this.drawPreview.QueueDraw();
            }

        this.iconDrawBuffer.DrawTo(this.wndDrawPanel, this.gcDrawPanel);

        if( this.menuitemGrid.Active )
            {
            this.drawGrid();
            }
        if( this.isDrawing )
            {
            this.drawShapeGuides();
            }
        }
    
    /// <summary>
    /// Refreshes the icon preview.
    /// </summary>
    /// <param name="o">Unused.</param>
    /// <param name="args">Unused.</param>
    protected void OnExposePreview(object o, ExposeEventArgs args)
        {
        this.iconCurrent.RenderPreview(this.drawPreview.GdkWindow, this.gcPreview, 128, 128);
        }

    protected void OnToolZoom(object sender, EventArgs args)
        {
        if(sender == this.toolZoomIn)
            {
            this.iconDrawBuffer.adjustZoom(1);
            this.setScrollBars();
            this.DoRedraw();
            }
        else if(sender == this.toolZoomOut)
            {
            this.iconDrawBuffer.adjustZoom(-1);
            this.setScrollBars();
            this.DoRedraw();
            }
        else if(sender == this.toolZoomBest)
            {
            int sizeWndX;
            int sizeWndY;
            this.wndDrawPanel.GetSize(out sizeWndX, out sizeWndY);
            int size = System.Math.Min(sizeWndX, sizeWndY);
            size = size / System.Math.Min(this.iconCurrent.width, this.iconCurrent.height);
            this.iconDrawBuffer.Zoom = size;
            this.setScrollBars();
            this.DoRedraw();
            }
        else
            {
            this.unimplementedFeature("Zoom mode");
            }
        }

    protected void OnToolPress(object sender, EventArgs args)
        {
        if(sender == this.toolPencil)
            {
            this.Mode = DrawMode.Pencil;
            }
        else if(sender == this.toolLine)
            {
            this.Mode = DrawMode.Line;
            }
        else if(sender == this.toolFlood)
            {
            this.Mode = DrawMode.Flood;
            }
        else if(sender == this.toolPipette)
            {
            this.Mode = DrawMode.Pipette;
            }
        else if(sender == this.toolRect)
            {
            this.Mode = DrawMode.Rect;
            }
        else if(sender == this.toolRectFilled)
            {
            this.Mode = DrawMode.FilledRect;
            }
        else if(sender == this.toolCircle)
            {
            this.Mode = DrawMode.Circle;
            }
        else if(sender == this.toolCircleFilled)
            {
            this.Mode = DrawMode.FilledCircle;
            }
        else if(sender == this.toolEraser)
            {
            this.Mode = DrawMode.Eraser;
            }
        else if(sender == this.toolEraserBig)
            {
            this.Mode = DrawMode.EraserBig;
            }

        else if(sender == this.toolSelect)
            {
            this.Mode = DrawMode.Select;
            }
        else
            this.unimplementedFeature("Tool event");
        }

    /// <summary>
    /// Colour was changed via the colour button.
    /// </summary>
    /// <param name="sender">The colour button.</param>
    /// <param name="args">Unused.</param>
    protected void OnColourChange(object sender, EventArgs args)
        {
        Gtk.ColorButton button = (Gtk.ColorButton)sender;
        Gdk.Color colour = new Gdk.Color(
            (byte)((button.Color.Red >> 8) & 0xff),
            (byte)((button.Color.Green >> 8) & 0xff),
            (byte)((button.Color.Blue >> 8) & 0xff));
        this.iconCurrent.colourCurrent = colour;
        this.palette.Colour = colour;
        this.drawColours.QueueDraw();
        }

    /// <summary>
    /// Checks whether there are unsaved changes the user want to keep.
    /// </summary>
    /// <returns><c>true</c>, if there is no need to save, <c>false</c> otherwise.</returns>
    private bool checkUnsaved()
        {
        if( this.isUnsaved )
            {
            MessageDialog dialog = new MessageDialog(
                this.wndMain, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo,
                "There are unsaved changes. Discard them?");
            int decision = dialog.Run();
            dialog.Destroy();
            if(decision == (int)ResponseType.No)
                return false;
            }
        return true;
        }

    /// <summary>
    /// Handles (most) menu item selections.
    /// </summary>
    /// <param name="sender">Selected menu item.</param>
    /// <param name="e">Unused.</param>
    protected void OnMenuItem(object sender, EventArgs e)
        {
        if(sender == this.menuitemNew)
            {
            if(this.checkUnsaved() == false)
                return;
            this.isUnsaved = false;
            this.iconCurrent = new Icon(64, 64, 8, this.drawEdit.GdkWindow);
            this.DoRedraw();
            this.drawPreview.QueueDraw();
            }
        else if(sender == this.menuitemOpen)
            {
            if(this.checkUnsaved() == false)
                return;
            Gtk.FileChooserDialog dialog = new FileChooserDialog(
                "Open..", this.wndMain, FileChooserAction.Open, 
                "Cancel", ResponseType.Cancel,
                "Open", ResponseType.Accept);
            if(dialog.Run() != (int)ResponseType.Accept)
                {
                dialog.Destroy();
                return;
                }
            byte[] bufFile = System.IO.File.ReadAllBytes(dialog.Filename);
            try
                {
                if(bufFile[0] != 0x89 || bufFile[1] != 0x50 || bufFile[2] != 0x4e || bufFile[3] != 0x47 ||
                    bufFile[4] != 0x0d || bufFile[5] != 0x0a || bufFile[6] != 0x1a || bufFile[7] != 0x0a)
                    {
                    Gtk.MessageDialog msg = new MessageDialog(
                        this.wndMain, DialogFlags.Modal, MessageType.Error, ButtonsType.Close,
                        "ERROR: Not a PNG file");
                    msg.Run();
                    msg.Destroy();
                    return;
                    }
                Gdk.Pixbuf pbLoaded = new Gdk.Pixbuf(bufFile);
                if(pbLoaded.Width != pbLoaded.Height || pbLoaded.Width > 256)
                    {
                    Gtk.MessageDialog msg = new MessageDialog(
                        this.wndMain, DialogFlags.Modal, MessageType.Error, ButtonsType.Close,
                        "ERROR: Icon too big to load");
                    msg.Run();
                    msg.Destroy();
                    return;
                    }
                else
                    {
                    //FIXME: NOT ROBUST!
                    this.iconCurrent = new Icon(pbLoaded.Width, pbLoaded.Height, 8, this.drawEdit.GdkWindow);
                    pbLoaded.CopyArea(0, 0, pbLoaded.Width, pbLoaded.Height, this.iconCurrent.icon.handle, 0, 0);
                    this.iconCurrent.refreshScratch();
                    this.DoRedraw();
                    }
                }
            catch(GLib.GException ex)
                {
                //FIXME: i don't think this can be caught..
                Gtk.MessageDialog msg = new MessageDialog(
                    this.wndMain, DialogFlags.Modal, MessageType.Error, ButtonsType.Close,
                    "ERROR: Corrupted file");
                msg.Run();
                msg.Destroy();
                return;
                }
            finally
                {
                dialog.Destroy();
                }
            }
        else if(sender == this.menuitemSave)
            {
            if(this.checkUnsaved() == false || this.strFilename == null)
                return;
            byte[] bufFile = this.iconCurrent.icon.handle.SaveToBuffer("png");
            System.IO.File.WriteAllBytes(this.strFilename, bufFile);
            }
        else if(sender == this.menuitemSaveAs)
            {
            Gtk.FileChooserDialog dialog = new FileChooserDialog(
                "Save As..", this.wndMain, FileChooserAction.Save,
                "Cancel", ResponseType.Cancel,
                "Save", ResponseType.Accept);
            if( dialog.Run() == (int)ResponseType.Accept )
                {
                byte[] bufFile = this.iconCurrent.icon.handle.SaveToBuffer("png");
                System.IO.File.WriteAllBytes(dialog.Filename, bufFile);
                }
            dialog.Destroy();
            }
        else if(sender == this.menuitemExit)
            {
            if(this.isUnsaved)
                {
                MessageDialog dialog = new MessageDialog(
                    this.wndMain, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo,
                    "There are unsaved changes. Sure you want to Quit?");
                int decision = dialog.Run();
                dialog.Destroy();
                if(decision == -8)
                    Application.Quit();
                }
            else
                Application.Quit();
            }
        else if(sender == this.menuitemCut || sender == this.menuitemCopy)
            {
            int posStartX = Math.Min(posStart.X, posEnd.X);
            int posStartY = Math.Min(posStart.Y, posEnd.Y);
            int posEndX = Math.Max(posStart.X, posEnd.X);
            int posEndY = Math.Max(posStart.Y, posEnd.Y);
            int sizeX = posEndX - posStartX;
            int sizeY = posEndY - posStartY;
            if(this.isDrawing && this.mode == DrawMode.Select)
                {
                this.isDrawing = false;
                this.DoRedraw();
                }
            int ox = posStartX / this.iconDrawBuffer.Zoom;
            int oy = posStartY / this.iconDrawBuffer.Zoom;
            int sx = sizeX / this.iconDrawBuffer.Zoom;
            int sy = sizeY / this.iconDrawBuffer.Zoom;
            if(sx == 0)
                sx = 1;
            if(sy == 0)
                sy = 1;

            this.pbClipboard = new Gdk.Pixbuf(this.iconCurrent.icon.handle, ox, oy, sx, sy);
            this.pbClipboard = this.pbClipboard.Copy();

            if(sender == this.menuitemCut)
                {
                this.iconCurrent.clearRegion(
                    posStartX / this.iconDrawBuffer.Zoom,
                    posStartY / this.iconDrawBuffer.Zoom,
                    sizeX / this.iconDrawBuffer.Zoom,
                    sizeY / this.iconDrawBuffer.Zoom
                );
                this.isDrawing = false;
                this.drawPreview.QueueDraw();
                this.DoRedraw();
                }
            }
        else if(sender == this.menuitemPaste)
            {
            this.Mode = DrawMode.Paste;
            this.isDrawing = true;
            }
        else if(sender == this.menuitemDelete)
            {
            if(this.mode == DrawMode.Select)
                {
                int posStartX = Math.Min(posStart.X, posEnd.X);
                int posStartY = Math.Min(posStart.Y, posEnd.Y);
                int posEndX = Math.Max(posStart.X, posEnd.X);
                int posEndY = Math.Max(posStart.Y, posEnd.Y);
                int sizeX = posEndX - posStartX;
                int sizeY = posEndY - posStartY;
                this.iconCurrent.clearRegion(
                    posStartX / this.iconDrawBuffer.Zoom,
                    posStartY / this.iconDrawBuffer.Zoom,
                    sizeX / this.iconDrawBuffer.Zoom,
                    sizeY / this.iconDrawBuffer.Zoom
                );
                this.isDrawing = false;
                this.drawPreview.QueueDraw();
                this.DoRedraw();
                }
            }
        }
    
    /// <summary>
    /// Handles the New sub-menu.
    /// </summary>
    /// <param name="objSource">Selected sub-menu item.</param>
    /// <param name="args">Unused.</param>
    protected void OnMenuNew(object objSource, EventArgs args)
        {
        if(this.isUnsaved)
            {
            MessageDialog dialog = new MessageDialog(
                this.wndMain, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo,
                "There are unsaved changes. Discard them?");
            int decision = dialog.Run();
            dialog.Destroy();
            if(decision == -9)
                return;
            }
        if(objSource == this.menuNew16)
            this.iconCurrent = new Icon(16, 16, 8, this.drawEdit.GdkWindow);
        else if(objSource == this.menuNew24)
            this.iconCurrent = new Icon(24, 24, 8, this.drawEdit.GdkWindow);
        else if(objSource == this.menuNew32)
            this.iconCurrent = new Icon(32, 32, 8, this.drawEdit.GdkWindow);
        else if(objSource == this.menuNew48)
            this.iconCurrent = new Icon(48, 48, 8, this.drawEdit.GdkWindow);
        else if(objSource == this.menuNew64)
            this.iconCurrent = new Icon(64, 64, 8, this.drawEdit.GdkWindow);
        else if(objSource == this.menuNew256)
            this.iconCurrent = new Icon(256, 256, 8, this.drawEdit.GdkWindow);
        else
            this.unimplementedFeature("New icon size");

        this.isUnsaved = false;
        this.setScrollBars();
        this.DoRedraw();
        this.drawPreview.QueueDraw();
        }

    protected void OnMenuAbout(object obj, EventArgs args)
        {
        // days since jan 2000; seconds since midnight / 2
        Version asmVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        Gtk.AboutDialog ab = new Gtk.AboutDialog();
        ab.ProgramName = "IconSharp";
        ab.Comments = 
            string.Format(
                " v{0}.{1}beta (build {2}.{3}) \n\n", 
                asmVersion.Major, asmVersion.Minor,
                asmVersion.Build, asmVersion.Revision) +
                "\n" +
                "\n\nThis program is released under the terms of " +
                "the GPL v2.";
                
        ab.Logo = new Gdk.PixbufLoader(
            System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(
                "logo.png")
            ).Pixbuf;
        ab.Copyright = "remy.horton@gmail.com";
        ab.Website = "http://software.remynet.org/iconsharp";
        ab.Run();
        ab.Destroy();
        }
    
    /// <summary>
    /// Handles Grid menu item.
    /// </summary>
    /// <param name="obj">Unused.</param>
    /// <param name="args">Unused.</param>
    /// <description>
    /// This just causes a refresh, as the drawing code gets
    /// the grid status direct from the menu item.
    /// </description>
    protected void OnMenuGrid(object obj, EventArgs args)
        {
        this.drawEdit.QueueDraw();
        }

    protected void OnDeleteEvent (object sender, DeleteEventArgs a)
        {
        Application.Quit ();
        a.RetVal = true;
        }

    protected void OnMenuUndo(object sender, EventArgs argv)
        {
        this.iconCurrent.undoDraw();
        this.DoRedraw();
        this.drawPreview.QueueDraw();
        }

    protected void OnMenuRedo(object sender, EventArgs argv)
        {
        this.iconCurrent.RedoDraw();
        this.DoRedraw();
        this.drawPreview.QueueDraw();
        }
    }
}
