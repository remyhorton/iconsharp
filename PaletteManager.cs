// IconSharp icon editor
// Copyright (C) 2014, Remy Horton.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


using System;
using Gtk;

namespace IconSharp
{

/// <summary>
/// Colour selection palette.
/// </summary>
public class PaletteManager
    {
    public delegate void NewColourDelegate(Gdk.Color newColour);

    private Gtk.DrawingArea drawPalette;
    private Gdk.GC gcPalette;
    private Gdk.Color[] lstColours;
    private int cntSamples;
    private int cntSamplesInRow;
    private int sizeSpacing;
    private int sizeSample;
    private int sizeMarginTop;
    private int sizeMarginLeft;
    private int idxMouseDownRow;
    private int idxMouseDownCol;
    private int idxSelectedSample;
    private NewColourDelegate funcColourChanged;
    
    /// <summary>
    /// Initialises a new palette.
    /// </summary>
    /// <param name="ptrDrawingArea">Drawing area palette is rendered to.</param>
    /// <param name="ptrEventBox">Event box that supplies events to the palette.</param>
    /// <param name="funcChange">Delegate to call upon colour changes.</param>
    /// <description>
    /// This takes a reference to a Gtk.DrawingArea rather than deriving it 
    /// as this is the easiest way to handle widget placement using Glade.
    /// </description>
    public PaletteManager(Gtk.DrawingArea ptrDrawingArea, Gtk.EventBox ptrEventBox, NewColourDelegate funcChange)
        {
        this.funcColourChanged = funcChange;
        this.drawPalette = ptrDrawingArea;
        this.gcPalette = new Gdk.GC(this.drawPalette.GdkWindow);

        this.drawPalette.ExposeEvent += this.DoExpose;

        ptrEventBox.ButtonPressEvent += this.DoMouseDown;
        ptrEventBox.ButtonReleaseEvent += this.DoMouseUp;

        this.sizeSpacing = 8;
        this.sizeSample = 16;
        this.cntSamples = 256;
        this.sizeMarginTop = 12;
        this.sizeMarginLeft = 12;
        this.cntSamplesInRow = 4;
        this.idxSelectedSample = 0;

        this.drawPalette.WidthRequest = 
            (this.sizeSample + this.sizeSpacing) * this.cntSamplesInRow + (this.sizeMarginLeft * 2);

        this.drawPalette.HeightRequest = 
            (this.sizeSample + this.sizeSpacing) * (this.cntSamples / this.cntSamplesInRow)
            + (this.sizeMarginTop * 2);

        this.lstColours = new Gdk.Color[this.cntSamples];
        int valColour = 0;
        for(int idxColour=0; idxColour<this.cntSamples; idxColour++)
            {
            this.lstColours[idxColour] = new Gdk.Color(
                (byte)((valColour >> 0) & 0xff),
                (byte)((valColour >> 8) & 0xff),
                (byte)((valColour >> 16) & 0xff));
            valColour += 6550;
            }
        }

    /// <summary>
    /// Get last selected colour
    /// </summary>
    /// <value>Most recent colour.</value>
    /// <description>
    /// This is called by the delegate that is notified of colour changes.
    /// </description>
    public Gdk.Color Colour
        {
        set
            {
            int idxSample = this.idxMouseDownRow * this.cntSamplesInRow + this.idxMouseDownCol;
            this.lstColours[idxSample] = value;
            }
        }

    /// <summary>
    /// Mouse pressed.
    /// </summary>
    /// <param name="objSource">Unused.</param>
    /// <param name="args">Mouse arguments (used to get mouse position).</param>
    /// <description>Stores which sample the click started on.</description>
    protected void DoMouseDown(object objSource, ButtonPressEventArgs args)
        {
        this.idxMouseDownRow = (Convert.ToInt32(args.Event.Y) - this.sizeMarginTop) 
            / (this.sizeSample + this.sizeSpacing);
        this.idxMouseDownCol = (Convert.ToInt32(args.Event.X) - this.sizeMarginLeft)
            / (this.sizeSample + this.sizeSpacing);
        }

    /// <summary>
    /// Mouse released.
    /// </summary>
    /// <param name="objSource">Unused.</param>
    /// <param name="args">Mouse arguments (used to get mouse position).</param>
    /// <description>
    /// Checks that both the press and release of a mouse click were on the
    /// same colour, and if so calls the colour setting delegate.
    /// </description>
    protected void DoMouseUp(object objSource, ButtonReleaseEventArgs args)
        {
        int posRow = (Convert.ToInt32(args.Event.Y) - this.sizeMarginTop)
            / (this.sizeSample + this.sizeSpacing);
        int posCol = (Convert.ToInt32(args.Event.X) - this.sizeMarginLeft) 
            / (this.sizeSample + this.sizeSpacing);
        if(posRow != this.idxMouseDownRow || posCol != this.idxMouseDownCol)
            return;
        int idxSample = posRow * this.cntSamplesInRow + posCol;

        this.idxSelectedSample = idxSample;
        this.drawPalette.QueueDraw();
        this.funcColourChanged(this.lstColours[idxSample]);
        }

    /// <summary>
    /// Redraws the palette.
    /// </summary>
    /// <param name="o">Unused.</param>
    /// <param name="args">Unused.</param>
    protected void DoExpose(object o, ExposeEventArgs args)
        {
        Gdk.Window wndPalette = this.drawPalette.GdkWindow;
        Gdk.GC gcPalette = this.gcPalette;
        Gdk.Color colorSampleBorder = new Gdk.Color(0, 0, 255);

        int posX = this.sizeMarginLeft;
        int posY = this.sizeMarginTop;
        int cntInRow = 0;
        int idxColour = 0;

        this.gcPalette.SetLineAttributes(1, Gdk.LineStyle.Solid, Gdk.CapStyle.Butt, Gdk.JoinStyle.Miter);
        for(int idxSample=0; idxSample < this.cntSamples; idxSample++)
            {
            gcPalette.RgbFgColor = this.lstColours[idxColour++];
            wndPalette.DrawRectangle(gcPalette, true, posX, posY, this.sizeSample, this.sizeSample);

            if(idxSample == this.idxSelectedSample)
                {
                gcPalette.RgbFgColor = colorSampleBorder;
                wndPalette.DrawRectangle(gcPalette, false, posX - 2, posY - 2, this.sizeSample + 3, this.sizeSample + 3);
                }
            if(++cntInRow < 4) 
                posX += this.sizeSample + this.sizeSpacing;
            else
                {
                cntInRow = 0;
                posX = this.sizeMarginLeft;
                posY += this.sizeSample + this.sizeSpacing;
                }
            }
        }

    }
}

