// IconSharp icon editor
// Copyright (C) 2014, Remy Horton.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


using System;
using Gtk;
using System.Collections.Generic;

namespace IconSharp
{

/// <summary>
/// Individual pixel RGBA values.
/// </summary>
struct Pixel
    {
    public Pixel(byte red, byte grn, byte blu) : this(red,grn,blu,255)
        {
        }
    public Pixel(byte red, byte grn, byte blu, byte alp) : this(red,grn,blu)
        {
        this.Red = red;
        this.Green = grn;
        this.Blue = blu;
        this.Alpha = alp;
        }
    public byte Red   { get; set; }
    public byte Green { get; set; }
    public byte Blue  { get; set; }
    public byte Alpha { get; set; }
    }

/// <summary>
/// Old and new RGBA for pixel at given location.
/// </summary>
struct PixelDelta
    {
    public Pixel Old;
    public Pixel New;
    public int X;
    public int Y;
    }

/// <summary>
/// Pixel changes between two images.
/// </summary>
/// <description>
/// Both old and new values are stored to allow both undo and redo.
/// These are intended to be chained up as a linked list.
/// </description>
class PixelDeltas
    {
    public List<PixelDelta> deltas;
    public PixelDeltas prev;
    public PixelDeltas next;
    public PixelDeltas(List<PixelDelta> lstDeltas)
        {
        this.deltas = lstDeltas;
        }
    }

/// <summary>
/// Wraps a bitmap and associated RGBA array.
/// </summary>
/// <description>
/// Internally Bitmaps are stored an a manually-allocated RGBA array, 
/// but need to be associated with a Gdk.Pixbuf in order to use 
/// many of GTK's bitmap functions. This conveniently wraps these
/// all up toghether.
/// </description>
class Bitmap
    {
    private byte[] pixels;
    public Gdk.Pixbuf handle;

    /// <summary>
    /// Initialises a new bitmap image.
    /// </summary>
    /// <param name="width">Bitmap width.</param>
    /// <param name="height">Bitmap height.</param>
    public Bitmap(int width, int height)
        {
        this.pixels = new byte[width * height * 4];
        this.handle = new Gdk.Pixbuf(this.pixels, true, 8, width, height, width * 4);
        this.Clear();
        }

    /// <summary>
    /// Gets or sets colour component at given coordinates.
    /// </summary>
    /// <param name="x">X coordinate.</param>
    /// <param name="y">Y coordinate.</param>
    /// <param name="idxColour">Colour component. 1=Red, 2=Green, 3=Blue, 4=Alpha.</param>
    public byte this [int x, int y, int idxColour]
        {
        get
            {
            int row = y * this.Width;
            return this.pixels[((row + x) * 4) + idxColour];
            }
        set
            {
            int row = y * this.Width;
            this.pixels[((row + x) * 4) + idxColour] = value;
            }
        }

    /// <summary>
    /// Sets entire bitmap to black
    /// </summary>
    public void Clear()
        {
        for(int y=0; y<this.Height; y++)
            for(int x=0; x<this.Width; x++)
                {
                int row = y * this.Width;
                this.pixels[((row + x) * 4) + 0] = 0;
                this.pixels[((row + x) * 4) + 1] = 0;
                this.pixels[((row + x) * 4) + 2] = 0;
                this.pixels[((row + x) * 4) + 3] = 0;
                }
        }

    public void setPixel(int x, int y, int red, int green, int blue, int alpha)
        {
        int row = y * this.Width;
        this.pixels[((row + x) * 4) + 0] = (byte)red;
        this.pixels[((row + x) * 4) + 1] = (byte)green;
        this.pixels[((row + x) * 4) + 2] = (byte)blue;
        this.pixels[((row + x) * 4) + 3] = (byte)alpha;
        }

    public Gdk.Color getPixel(int x, int y)
        {
        int row = y * this.Width;
        return new Gdk.Color(
            this.pixels[((row + x) * 4) + 0],
            this.pixels[((row + x) * 4) + 1],
            this.pixels[((row + x) * 4) + 2]);
        }

    public void copyPixelData(Bitmap src)
        {
        for(int y=0; y<this.Height; y++)
            {
            int row = y * this.Width;
            for(int x=0; x<this.Width; x++)
                {
                this.pixels[((row + x) * 4) + 0] = src.pixels[((row + x) * 4) + 0];
                this.pixels[((row + x) * 4) + 1] = src.pixels[((row + x) * 4) + 1];
                this.pixels[((row + x) * 4) + 2] = src.pixels[((row + x) * 4) + 2];
                this.pixels[((row + x) * 4) + 3] = src.pixels[((row + x) * 4) + 3];
                }
            }
        }

    public int Width
        {
        get
            {
            return this.handle.Width;
            }
        }

    public int Height
        {
        get
            {
            return this.handle.Height;
            }
        }

    }

/// <summary>
/// Individual icon.
/// </summary>
/// <description>
/// This contains an icon, and the functions needed to turn shape coordinates 
/// into changed pixels. It uses a Pixmap to create a draw mask, which is then
/// overlaid onto the existing icon using the current colour. 
/// The orginial intention was to allow multiple icons to be edited at once, hene
/// why all the shape drawing is within member functions.
/// </description>
class Icon
    {
    public Bitmap icon;
    private Bitmap addMask;
    private Bitmap scratch;
    public int width;
    public int height;
    //private int depth;

    private Gdk.Pixmap pmRender;
    private Gdk.GC     gcRender;
    private Gdk.Color  colourWhite;
    public  Gdk.Color  colourCurrent;

    private PixelDeltas undoDeltas;
    private int cntChangedPixels;
    
    /// <summary>
    /// Initializes a new instance of the <see cref="IconSharp.Icon"/> class.
    /// </summary>
    /// <param name="width">Icon width.</param>
    /// <param name="height">Icon height.</param>
    /// <param name="depth">Icon pixel depth (unused).</param>
    /// <param name="reference">Gdk.Drawable to use as Pixmap reference.</param>
    public Icon(int width, int height, int depth, Gdk.Drawable reference)
        {
        this.icon = new Bitmap(width, height);
        this.addMask = new Bitmap(width, height);
        this.scratch = new Bitmap(width, height);
        this.width = width;
        this.height = height;
        //this.depth = depth;
        this.pmRender = new Gdk.Pixmap(reference, width, height, 24);
        this.gcRender = new Gdk.GC(this.pmRender);
        this.gcRender.RgbFgColor = new Gdk.Color(255, 255, 255);
        this.pmRender.DrawRectangle(this.gcRender, true, new Gdk.Rectangle(0, 0, this.width, this.height));
        //this.colourMask = new Gdk.Color(0, 0, 0);
        this.colourWhite = new Gdk.Color(255, 255, 255);
        this.colourCurrent = new Gdk.Color(0, 0, 0);
        this.undoDeltas = null;
        }

    /// <summary>
    /// Gets current icon (or expected icon appearance, if drawing).
    /// </summary>
    /// <returns>The pixel buff.</returns>
    /// <description>
    /// In the past, the scratch buffer was updated on demand. This code
    /// has been combined with the mask construction in <see cref="plotSlape"/>.
    /// </description>
    public Gdk.Pixbuf getPixelBuff()
        {
        return this.scratch.handle;
        }

    /// <summary>
    /// Reset scratch buffer.
    /// </summary>
    public void refreshScratch()
        {
        this.scratch.copyPixelData(this.icon);
        }

    /// <summary>
    /// Flood fill
    /// </summary>
    /// <param name="posRef">Position to beginf lood fill.</param>
    private void floodFill(Gdk.Point posRef)
        {
        byte red = this.icon[posRef.X,posRef.Y,0];
        byte grn = this.icon[posRef.X,posRef.Y,1];
        byte blu = this.icon[posRef.X,posRef.Y,2];
        byte alp = this.icon[posRef.X,posRef.Y,3];
        for(int y=0; y<this.height; y++)
            {
            for(int x=0; x<this.width; x++)
                this.scratch[x,y,0] = 0;
            }
        this.floodFillRec(posRef.X, posRef.Y, red, grn, blu, alp);
        }

    /// <summary>
    /// Recursive part of flood fill.
    /// </summary>
    /// <param name="x">The x coordinate.</param>
    /// <param name="y">The y coordinate.</param>
    /// <param name="r">The red component.</param>
    /// <param name="g">The green component.</param>
    /// <param name="b">The blue component.</param>
    /// <param name="a">The alpha component.</param>
    private void floodFillRec(int x, int y, byte r, byte g, byte b, byte a)
        {
        if(x < 0 || y < 0 || x >= this.width || y >= this.height)
            return;
        if(this.scratch[x,y,0] != 0)
            return;
            this.scratch[x,y,0] = 1;
            if(this.icon[x,y,0] == r &&
               this.icon[x,y,1] == g &&
               this.icon[x,y,2] == b &&
               this.icon[x,y,3] == a)
            {
            this.pmRender.DrawPoint(this.gcRender, x, y);
            this.floodFillRec(x - 1, y, r, g, b, a);
            this.floodFillRec(x + 1, y, r, g, b, a);
            this.floodFillRec(x, y - 1, r, g, b, a);
            this.floodFillRec(x, y + 1, r, g, b, a);
            }
        }

    /// <summary>
    /// Finds difference between icon and scratch buffer.
    /// </summary>
    private void makeUndoDelta()
        {
        List<PixelDelta> deltas = new List<PixelDelta>();
        for(int y=0; y<this.height; y++)
            for(int x=0; x<this.width; x++)
                {
                byte red1 = this.icon[x, y, 0];
                byte grn1 = this.icon[x, y, 1];
                byte blu1 = this.icon[x, y, 2];
                byte alp1 = this.icon[x, y, 3];
                byte red2 = this.scratch[x, y, 0];
                byte grn2 = this.scratch[x, y, 1];
                byte blu2 = this.scratch[x, y, 2];
                byte alp2 = this.scratch[x, y, 3];
                if(red1 != red2 || grn1 != grn2 || blu1 != blu2 || alp1 != alp2)
                    {
                    PixelDelta delta = new PixelDelta();
                    delta.Old.Red = red1;
                    delta.Old.Green = grn1;
                    delta.Old.Blue = blu1;
                    delta.Old.Alpha = alp1;
                    delta.New.Red = red2;
                    delta.New.Green = grn2;
                    delta.New.Blue = blu2;
                    delta.New.Alpha = alp2;
                    delta.X = x;
                    delta.Y = y;
                    deltas.Add(delta);
                    }
                }
        PixelDeltas changeset = new PixelDeltas(deltas);
        changeset.prev = this.undoDeltas;
        this.undoDeltas = changeset;
        }


    /// <summary>
    /// Plots the shape for given mode and parameters.
    /// </summary>
    /// <param name="mode">Drawing mode.</param>
    /// <param name="posStart">Start position for shape.</param>
    /// <param name="posEnd">End position for shape.</param>
    /// <param name="pbGlyph">Clipboard image (used when pasting).</param>
    /// <description>
    /// This updates the internal scratch buffer based on the parameters for the
    /// shape being drawn by the current mode. 
    /// </description>
    public void plotShape(DrawMode mode, Gdk.Point posStart, Gdk.Point posEnd, Gdk.Pixbuf pbGlyph)
        {
        int posStartX = Math.Min(posStart.X, posEnd.X);
        int posStartY = Math.Min(posStart.Y, posEnd.Y);
        int posEndX = Math.Max(posStart.X, posEnd.X);
        int posEndY = Math.Max(posStart.Y, posEnd.Y);
        int sizeX = posEndX - posStartX;
        int sizeY = posEndY - posStartY;

        this.refreshScratch();

        if(mode == DrawMode.Paste)
            {
            if(pbGlyph == null)
                return;
            int pasteX = Math.Min(pbGlyph.Width, this.scratch.Width - posEnd.X);
            int pasteY = Math.Min(pbGlyph.Height, this.scratch.Height - posEnd.Y);
            pbGlyph.Composite(
                this.scratch.handle,
                posEnd.X, posEnd.Y,
                pasteX, pasteY,
                posEnd.X, posEnd.Y, 
                1, 1, Gdk.InterpType.Nearest, 255
                );
            return;
            }

        if(mode != DrawMode.Pencil && mode != DrawMode.Eraser && mode != DrawMode.EraserBig)
            {
            this.gcRender.RgbFgColor = this.colourWhite;
            this.pmRender.DrawRectangle(this.gcRender, true, new Gdk.Rectangle(0, 0, this.width, this.height));
            }

        this.gcRender.RgbFgColor = new Gdk.Color(0, 0, 0);
        switch(mode)
            {
            case DrawMode.Pencil:
            case DrawMode.Eraser:
                this.pmRender.DrawPoint(this.gcRender, posEnd.X, posEnd.Y);
                break;

            case DrawMode.Line:
                this.gcRender.SetLineAttributes(
                    1, Gdk.LineStyle.Solid, Gdk.CapStyle.Projecting, Gdk.JoinStyle.Miter);
                //this.pmRender.DrawLine(this.gcRender, posStart.X, posStart.Y, posEnd.X, posEnd.Y);
                this.pmRender.DrawLines(this.gcRender, new Gdk.Point[] { posStart, posEnd });
                break;

            case DrawMode.Rect:
                this.gcRender.SetLineAttributes(
                    1, Gdk.LineStyle.Solid, Gdk.CapStyle.Butt, Gdk.JoinStyle.Miter);
                Gdk.Point[] lstPoints = new Gdk.Point[] {
                    new Gdk.Point( posStart.X, posStart.Y ),
                    new Gdk.Point( posEnd.X,   posStart.Y ),
                    new Gdk.Point( posEnd.X,   posEnd.Y ),
                    new Gdk.Point( posStart.X, posEnd.Y ),
                    new Gdk.Point( posStart.X, posStart.Y )
                };
                this.pmRender.DrawLines(this.gcRender, lstPoints);
                break;

            case DrawMode.FilledRect:
                this.gcRender.SetLineAttributes(
                    1, Gdk.LineStyle.Solid, Gdk.CapStyle.Butt, Gdk.JoinStyle.Miter);
                this.pmRender.DrawRectangle(this.gcRender, true, posStartX, posStartY, sizeX + 1, sizeY + 1);
                break;

            case DrawMode.EraserBig:
                this.gcRender.SetLineAttributes(
                    1, Gdk.LineStyle.Solid, Gdk.CapStyle.Butt, Gdk.JoinStyle.Miter);
                this.pmRender.DrawRectangle(this.gcRender, true, posEnd.X - 1, posEnd.Y - 1, 3, 3);
                break;

            case DrawMode.Circle:
                this.gcRender.SetLineAttributes(
                    1, Gdk.LineStyle.Solid, Gdk.CapStyle.Butt, Gdk.JoinStyle.Miter);
                this.pmRender.DrawArc(
                    this.gcRender, true, posStart.X, posStart.Y, sizeX, sizeY, 0, 64);
                break;

            case DrawMode.Select:
                break;

            case DrawMode.Flood:
                this.floodFill(posEnd);
                this.refreshScratch();
                break;

            default:
                throw new NotImplementedException();
            }

        this.cntChangedPixels = 0;
        this.addMask.handle.GetFromDrawable(
            this.pmRender, 
            this.pmRender.Colormap,
            0, 0, 0, 0, this.width, this.height);
        for(int y=0; y<this.height; y++)
            {
            for(int x=0; x<this.width; x++)
                {
                if(this.addMask[x, y, 0] == 0)
                    {
                    this.cntChangedPixels++;
                    if(mode == DrawMode.Eraser || mode == DrawMode.EraserBig)
                        {
                        this.scratch.setPixel(x, y, 255, 255, 255, 0);
                        }
                    else
                        {
                        this.scratch.setPixel(
                            x, y, 
                            this.colourCurrent.Red,
                            this.colourCurrent.Green,
                            this.colourCurrent.Blue, 255
                        );
                        }
                    }
                }
            }
        }

    /// <summary>
    /// Commits the shape.
    /// </summary>
    /// <param name="isErase">Unused.</param>
    /// <description>
    /// This creates undo data 
    /// </description>
    public void commitShape(bool isErase)
        {
        this.makeUndoDelta();
        this.icon.copyPixelData(this.scratch);
        this.gcRender.RgbFgColor = this.colourWhite;
        this.pmRender.DrawRectangle(this.gcRender, true, new Gdk.Rectangle(0, 0, this.width, this.height));
        }

    public void undoDraw()
        {
        // Don't allow undo of first action. Bit of a hack though..
        if(this.undoDeltas == null || this.undoDeltas.prev == null )
            return;
        foreach(PixelDelta delta in this.undoDeltas.deltas)
            {
            this.icon[delta.X, delta.Y, 0] = delta.Old.Red;
            this.icon[delta.X, delta.Y, 1] = delta.Old.Green;
            this.icon[delta.X, delta.Y, 2] = delta.Old.Blue;
            this.icon[delta.X, delta.Y, 3] = delta.Old.Alpha;
            }
        if(this.undoDeltas.prev != null)
            {
            this.undoDeltas.prev.next = this.undoDeltas;
            this.undoDeltas = this.undoDeltas.prev;
            }
        this.refreshScratch();
        }
    
    public void RedoDraw()
        {
        if(this.undoDeltas == null || this.undoDeltas.next == null)
            return;

            foreach(PixelDelta delta in this.undoDeltas.next.deltas)
                {
                this.icon[delta.X, delta.Y, 0] = delta.New.Red;
                this.icon[delta.X, delta.Y, 1] = delta.New.Green;
                this.icon[delta.X, delta.Y, 2] = delta.New.Blue;
                this.icon[delta.X, delta.Y, 3] = delta.New.Alpha;
                }

            this.undoDeltas = this.undoDeltas.next;
        this.refreshScratch();
        }

    /// <summary>
    /// Clears given region.
    /// </summary>
    /// <param name="posX">X offset of area to clear.</param>
    /// <param name="posY">Y  offset of area to clear.</param>
    /// <param name="width">Width of area to clear</param>
    /// <param name="height">Height of area to clear</param>
    public void clearRegion(int posX, int posY, int width, int height)
        {
        for(int y=posY; y<posY+height; y++)
            for(int x=posX; x<posX+width; x++)
                {
                this.scratch[x, y, 0] = 0;
                this.scratch[x, y, 1] = 0;
                this.scratch[x, y, 2] = 0;
                this.scratch[x, y, 3] = 0;
                }
        this.makeUndoDelta();
        this.icon.copyPixelData(this.scratch);
        }

    /// <summary>
    /// Renders preview to given window.
    /// </summary>
    /// <param name="wndTarget">Target window.</param>
    /// <param name="gcTarget">Graphics Context of target window.</param>
    /// <param name="wndWidth">Target window width.</param>
    /// <param name="wndHeight">Target indow height.</param>
    public void RenderPreview(Gdk.Window wndTarget, Gdk.GC gcTarget, int wndWidth, int wndHeight)
        {
        gcTarget.RgbFgColor = new Gdk.Color(192, 192, 192);
        wndTarget.DrawRectangle(gcTarget, true, new Gdk.Rectangle(0, 0, wndWidth, wndHeight));

        if(this.width <= wndWidth && this.height <= wndHeight)
            {
            int posX = (wndWidth - this.width) >> 1;
            int posY = (wndHeight - this.height) >> 1;
            gcTarget.RgbFgColor = new Gdk.Color(255, 255, 255);
            wndTarget.DrawRectangle(
                gcTarget, true, new Gdk.Rectangle(posX, posY, this.width, this.height));

            wndTarget.DrawPixbuf(
                gcTarget, this.scratch.handle, 
                0, 0, posX, posY, this.width, this.height,
                Gdk.RgbDither.None, 0, 0
            );
            }
        else
            {
            gcTarget.RgbFgColor = new Gdk.Color(255, 255, 255);
            wndTarget.DrawRectangle(gcTarget, true, new Gdk.Rectangle(0, 0, wndWidth, wndHeight));

            Gdk.Pixbuf pb = this.scratch.handle.CompositeColorSimple(
                wndWidth, wndHeight, Gdk.InterpType.Nearest, 255, 8, 0xffffff, 0xffffff);

            wndTarget.DrawPixbuf(gcTarget, pb, 0, 0, 0, 0, -1, -1, Gdk.RgbDither.None, 0, 0);
            }
        
        }

    public Gdk.Color getPixel(int x, int y)
        {
        return this.icon.getPixel(x, y);
        }
    }
}

