#!/bin/bash



DLLs=(
    `find /usr/lib/mono -iname gtk-sharp.dll`
    `find /usr/lib/mono -iname glib-sharp.dll`
    `find /usr/lib/mono -iname gdk-sharp.dll`
    `find /usr/lib/mono -iname glade-sharp.dll`
    `find /usr/lib/mono -iname pango-sharp.dll`
    `find /usr/lib/mono -iname atk-sharp.dll`
    System.dll Mono.Posix.dll System.Core.dll
)
RESs=(
    iconPencil.png
    iconCircle.png
    iconCircleFilled.png 
    iconRect.png 
    iconRectFilled.png 
    iconEraser.png 
    iconSelect.png 
    iconFlood.png 
    iconLine.png 
    gui.glade 
    iconEraserBig.png 
    logo.png 
)
OPTs=(
#    -noconfig 
#    -nostdlib
#    -nologo
    -warn:4
#    /debug:full
    -optimize+
    -codepage:utf8
    -platform:x86
    -t:winexe
)
SRCs=(
    Program.cs 
    Properties/AssemblyInfo.cs 
    DrawBuffer.cs
    PaletteManager.cs 
    Icon.cs
)
BIN=IconSharp.exe

CMD=""

for opt in "${OPTs[@]}"; do
    CMD=$CMD" ${opt}"
done;

for dll in "${DLLs[@]}"; do
    CMD=$CMD" /r:${dll}"
done;

for res in "${RESs[@]}"; do
    CMD=$CMD" /res:${res},${res}"
done;

for src in "${SRCs[@]}"; do
    CMD=$CMD" ${src}"
done;

dmcs $CMD /out:$BIN